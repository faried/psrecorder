#!/usr/bin/env python
"""A simple web server that accepts files and saves them."""

import os
import os.path
from pprint import pprint
import time

import web

# directory to save images in
SAVEDIR = 'data'
URLS = (
    '/', 'index',
    '/save', 'saveimage'
)

class index(object):
    """Site root."""

    def GET(self):
        return 'hi there!'


class saveimage(object):
    """Save a POSTed image."""

    def GET(self):
        return 'nothing to see here'

    def POST(self):
        # pprint(web.ctx.items())
        i = web.input(image={})
        if 'image' in i:
            # prepend time to filename
            fname = os.path.join(SAVEDIR, str(time.time()) + i.image.filename)
            print('writing to %s' % fname)
            fout = file(fname, 'w')
            fout.write(i.image.file.read())
            fout.close()

        return 'ok'


if __name__ == '__main__':
    # set this to False in production
    web.config.debug = False
    app = web.application(URLS, globals())
    app.run()

# eof
