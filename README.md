PSRecorder:

* android application to record videos, one after the other
* the user can touch the screen (or the button) to take a snapshot of the video for upload
* uses a background (bound) service to upload the pictures
* simple server in python to store uploaded pictures

Uses http://kevinsawicki.github.io/http-request/ to simplify uploads.

Tested on 4.2.2 (Nexus 4).  Set to use Android 2.3.3 at minimum.

On the Nexus S, it doesn't like having the preview set to 640x480: it won't
switch to 480p for recording.  This probably affects other devices as well.
