package pk.node.psrecorder;

// constants used in several places
public class Const
{
    public static String TAG = "PSRecoder";

    // probably should be an enum
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    // how long to record each video for, in milliseconds (1000 * seconds)
    public static final int VIDEO_LENGTH = 10 * 1000;

    // for the service
    // where to send the frames
    // 192.168.1.90 is my laptop's IP address at home
    public static final String POST_URL = "http://192.168.1.90:8080/save";
    // try sending pictures every 10 seconds
    public static final int TIMER_RETRY = 10 * 1000;
    // probably should be an enum, too
    public static final int MESSAGE_SEND_FRAME = 0;

    // bundle keys, for passing messages between the app and the service
    public static final String VIDEO_FILENAME = "video_filename";
    public static final String VIDEO_TIME = "video_time";

}
