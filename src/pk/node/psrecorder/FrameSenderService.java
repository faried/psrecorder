package pk.node.psrecorder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.github.kevinsawicki.http.HttpRequest;

public class FrameSenderService extends Service
{
    private static ConnectivityManager mCM = null;
    private static FrameSenderService instance;

    // timer to run in the background to send events
    private static Timer mTimer = new Timer();
    // message handler, receives messages from the application
    private static final Messenger mMessenger = new Messenger(new MessageHandler());

    // files to upload
    private List<File> mFiles = new ArrayList<File>();

    @Override
    public IBinder onBind(Intent intent)
    {
        return mMessenger.getBinder();
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
        mCM = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        mTimer.schedule(new SendFrames(), 0, Const.TIMER_RETRY);
    }

    @Override
    public void onDestroy()
    {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }

        mCM = null;
    }

    private static class MessageHandler extends Handler
    {
        @Override
        public void handleMessage(final Message msg)
        {
            switch (msg.what) {
            case Const.MESSAGE_SEND_FRAME:
                getFrame(msg.getData());
                break;
            default:
                Log.e(Const.TAG, "unknown message: " + msg.what);
            }
        }

        private static void getFrame(final Bundle b)
        {
            MediaMetadataRetriever mRetriever = new MediaMetadataRetriever();
            Log.v(Const.TAG, "-------- trying to set data source to " + b.getString(Const.VIDEO_FILENAME));
            mRetriever.setDataSource(b.getString(Const.VIDEO_FILENAME));
            Bitmap bmap = mRetriever.getFrameAtTime(b.getLong(Const.VIDEO_TIME) * 1000,
                    MediaMetadataRetriever.OPTION_CLOSEST);
            if (bmap != null) {
                Log.v(Const.TAG, "-------- found bitmap!");
                Log.v(Const.TAG, "-------- size: " + bmap.getWidth() + "x" + bmap.getHeight());
                File outfile = Files.getOutputMediaFile(Const.MEDIA_TYPE_IMAGE);
                try {
                    FileOutputStream fos = new FileOutputStream(outfile);
                    bmap.compress(Bitmap.CompressFormat.JPEG, 85, fos);
                    fos.close();
                } catch (FileNotFoundException e) {
                    Log.e(Const.TAG, "can't happen: file not found for " + outfile.getName() + ":" + e.getMessage());
                } catch (IOException e) {
                    Log.e(Const.TAG, "shouldn't happen: cannot close " + outfile.getName() + ": " + e.getMessage());
                } finally {
                    // should synchronize this
                    instance.mFiles.add(outfile);
                }
            }
        }
    }

    private class SendFrames extends TimerTask
    {
        private boolean mSending = false;

        @Override
        public void run()
        {
            // already sending
            if (mSending == true) return;
            // don't do anything if we can't use the network
            if (!mCM.getActiveNetworkInfo().isConnected()) return;
            // nothing to do
            if (instance.mFiles.size() == 0) return;

            mSending = true;

            List<File> removeFiles = new ArrayList<File>();

            for (File file : instance.mFiles) {
                Log.v(Const.TAG, "-------- have to send: " + file.getName());
                try {
                    HttpRequest request = HttpRequest.post(Const.POST_URL);
                    request.part("image", file.getName(), file);
                    if (request.ok()) System.out.println("Status was updated");
                    removeFiles.add(file);
                } catch (Exception e) {
                    Log.e(Const.TAG, "-------- something went wrong with the upload: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            for (int i = 0; i < removeFiles.size(); i++)
                instance.mFiles.remove(removeFiles.get(i));
            removeFiles = null;

            mSending = false;
        }
    }
}
