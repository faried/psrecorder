package pk.node.psrecorder;

import java.io.IOException;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * A basic Camera preview class from
 * http://developer.android.com/guide/topics/media/camera.html#custom-camera
 */
public class CameraView extends SurfaceView implements SurfaceHolder.Callback
{
    private SurfaceHolder mHolder;
    private Camera mCamera;

    public CameraView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public CameraView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CameraView(Context context)
    {
        super(context);
    }

    public CameraView(Context context, Camera camera)
    {
        super(context);
        initialize(camera);
    }

    @SuppressWarnings("deprecation")
    public void initialize(Camera camera)
    {
        Log.v(Const.TAG, "-------- CameraView initialize");
        mCamera = camera;

        this.setClickable(true);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder)
    {
        Log.v(Const.TAG, "-------- CameraView surface created");
        // The Surface has been created, now tell the camera where to draw the
        // preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(Const.TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder)
    {
        Log.v(Const.TAG, "-------- CameraView surface destroyed");
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
        Log.v(Const.TAG, "-------- CameraView surface changed");
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // if possible, set the preview size to 640x480
        // alter to fit your camera size.
        Camera.Parameters params = mCamera.getParameters();
        boolean sizefound = false;
        for (Camera.Size size : params.getSupportedPreviewSizes())
            if (size.width == 640 && size.height == 480) {
                Log.v(Const.TAG, "-------- supported preview size: " + size.width + "x" + size.height);
                sizefound = true;
                break;
            }

        if (sizefound) {
            mHolder.setFixedSize(640, 480);
            params.setPreviewSize(640, 480);
            mCamera.setParameters(params);
        }

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            Log.d(Const.TAG, "Error starting camera preview: " + e.getMessage());
        }
    }
}