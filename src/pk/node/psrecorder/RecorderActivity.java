package pk.node.psrecorder;

import java.io.File;
import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

/*
 * read:
 *   http://developer.android.com/guide/topics/media/camera.html#custom-camera
 */

public class RecorderActivity extends Activity implements OnClickListener
{
    // the camera object (single object for the app)
    private Camera mCamera;
    // this does all the real recording work
    private MediaRecorder mMediaRecorder;
    // the preview view of what we're recording
    private CameraView mPreview;

    private Button mCaptureButton;
    private Button mSendFrameButton;

    // file to save in
    private File mCurrentFile;

    // handler for processing messages to start/stop recording
    // we use this to stop recording after a certain number of seconds
    // and then create a new file
    private Handler mHandler;

    // are we recording?
    private boolean mIsRecording;
    // does the user want us to stop recording?
    private boolean mStopRecording;

    // if we want to send off a snapshot of a frame, we need to
    // know when the frame occurred after the movie started recording
    // this variable is set whenever we start recording
    private long mStartRecordingTime;

    // for the service to send the snapshots
    private Messenger mService;
    private boolean mBound;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorder);

        mCamera = getCameraInstance();
        if (mCamera == null) {
            Log.e(Const.TAG, "cannot get camera!");
            finish();
        }
        setCameraParameters();

        mPreview = new CameraView(this, mCamera);
        mPreview.setOnClickListener(this);

        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);

        mIsRecording = false;
        mStopRecording = false;

        mCaptureButton = (Button) findViewById(R.id.button_capture);
        mCaptureButton.setOnClickListener(this);
        mSendFrameButton = (Button) findViewById(R.id.button_sendframe);
        mSendFrameButton.setOnClickListener(this);

        mHandler = new Handler();

        Intent intent = new Intent(this, FrameSenderService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        mBound = false;
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        releaseMediaRecorder(); // if you are using MediaRecorder, release it
        releaseCamera(); // release the camera immediately on pause event
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.recorder, menu);
        return true;
    }

    @Override
    public void onClick(View v)
    {
        Log.v(Const.TAG, "view touched: " + v.toString());
        if (v == mCaptureButton) captureButtonPressed();
        else if (v == mSendFrameButton) sendFrameButtonPressed();
        else if (v == mPreview) sendFrameButtonPressed();
    }

    private void sendFrameButtonPressed()
    {
        if (mIsRecording) {
            final long diff = (System.currentTimeMillis() - mStartRecordingTime);
            final String fname = mCurrentFile.getAbsolutePath();
            Toast.makeText(this, "seconds after recording began: " + diff / 1000, Toast.LENGTH_SHORT).show();
            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    Bundle b = new Bundle();
                    b.putString(Const.VIDEO_FILENAME, fname);
                    b.putLong(Const.VIDEO_TIME, diff);
                    Message msg = Message.obtain(null, Const.MESSAGE_SEND_FRAME);
                    msg.setData(b);
                    try {
                        mService.send(msg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        Log.e(Const.TAG, e.getMessage());
                    }
                }
            }, Const.VIDEO_LENGTH);
        }
    }

    private void captureButtonPressed()
    {
        if (mIsRecording) {
            mStopRecording = true;
            stopRecording();
        } else startRecording();
    }

    private void stopRecording()
    {
        // if stopRecording is called using the handler
        // after the user has manually stopped recording,
        // this line will prevent it from trying to stop the
        // recorder
        if (!mIsRecording) return;

        // stop recording and release camera
        Log.v(Const.TAG, "-------- about to stop recorder");
        try {
            mMediaRecorder.stop(); // stop the recording
            Log.v(Const.TAG, "-------- stopped media recorder");
        } catch (RuntimeException e) {
            Log.e(Const.TAG, "-------- e is " + e.getMessage());
        }

        releaseMediaRecorder(); // release the MediaRecorder object
        Log.v(Const.TAG, "-------- going to lock camera");
        mCamera.lock();
        Log.v(Const.TAG, "-------- camera locked!");

        // inform the user that recording has stopped
        mCaptureButton.setText(R.string.action_capture);
        mIsRecording = false;

        // if the user doesn't want us to stop, we start recording to a new file
        // immediately
        if (!mStopRecording) mHandler.post(new Runnable()
        {
            public void run()
            {
                startRecording();
            }
        });
    }

    private void startRecording()
    {
        // shouldn't happen: if we're recording, return
        if (mIsRecording) return;

        // initialize video camera
        if (prepareVideoRecorder()) {
            // Camera is available and unlocked, MediaRecorder is prepared,
            // now you can start recording
            mMediaRecorder.start();
            mStartRecordingTime = System.currentTimeMillis();

            // inform the user that recording has started
            mCaptureButton.setText(R.string.action_stopcapture);
            mIsRecording = true;
            Toast.makeText(this, "recording to " + mCurrentFile.getName(), Toast.LENGTH_SHORT).show();
            // stop recording after a certain amount of time
            mHandler.postDelayed(new Runnable()
            {
                public void run()
                {
                    stopRecording();
                }
            }, Const.VIDEO_LENGTH);
        } else {
            // prepare didn't work, release the camera
            releaseMediaRecorder();
            Toast.makeText(this, "something went wrong -- cannot prepare video recorder", Toast.LENGTH_LONG).show();
        }
    }

    static Camera getCameraInstance()
    {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            Log.e(Const.TAG, "cannot get camera: " + e.getMessage());
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private void setCameraParameters()
    {
        Camera.Parameters params = mCamera.getParameters();

        // set the focus mode
        List<String> focusModes = params.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) params
                .setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        else if (focusModes.contains(Camera.Parameters.FOCUS_MODE_INFINITY)) params
                .setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
        else if (focusModes.contains(Camera.Parameters.FOCUS_MODE_FIXED)) params
                .setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);

        // params.setRecordingHint(true);

        // set Camera parameters
        mCamera.setParameters(params);
    }

    private boolean prepareVideoRecorder()
    {
        Log.v(Const.TAG, "-------- prepare video recorder");
        if (mCamera == null) mCamera = getCameraInstance();
        mMediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        // 480p
        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_480P));
        // 720p
        // mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_720P));
        // automatically select low quality, whatever that is
        // mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
        // automatically select high quality
        // mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));

        // Step 4: Set output file
        mCurrentFile = Files.getOutputMediaFile(Const.MEDIA_TYPE_VIDEO);
        mMediaRecorder.setOutputFile(mCurrentFile.toString());

        // Step 5: Set the preview output
        mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        // Step 6: Prepare configured MediaRecorder
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.e(Const.TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.e(Const.TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder()
    {
        Log.v(Const.TAG, "-------- release media recorder");
        if (mMediaRecorder != null) {
            Log.v(Const.TAG, "-------- release media recorder was not null");
            mMediaRecorder.reset(); // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();
        }
        Log.v(Const.TAG, "-------- release media recorder done");
    }

    private void releaseCamera()
    {
        if (mCamera != null) {
            mCamera.release(); // release the camera for other applications
            mCamera = null;
        }
    }

    // saving files

    // talking to the service
    // Defines callbacks for service binding, passed to bindService()
    final ServiceConnection mConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(final ComponentName className, final IBinder service)
        {
            // This is called when the connection with the service has
            // been established, giving us the object we can use to
            // interact with the service. We communicate with the service
            // using a Messenger.
            Log.d(Const.TAG, "-------- service: " + service.toString());
            if (mService == null) {
                Log.d(Const.TAG, "-------- new messenger");
                mService = new Messenger(service);
            }
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(final ComponentName className)
        {
            mService = null;
            mBound = false;
            Log.d(Const.TAG, "-------- service disconnected");
        }
    };
}
